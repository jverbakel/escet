//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2023 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

include::_root_attributes.asciidoc[]

indexterm:[legal]

[[legal-chapter-index]]
== Legal

The material in this documentation is Copyright (c) 2010, 2023 Contributors to the Eclipse Foundation.

Eclipse ESCET and ESCET are trademarks of the Eclipse Foundation.
Eclipse, and the Eclipse Logo are registered trademarks of the Eclipse Foundation.
Other names may be trademarks of their respective owners.

*License*

The Eclipse Foundation makes available all content in this document ("Content").
Unless otherwise indicated below, the Content is provided to you under the terms and conditions of the MIT License.
A copy of the MIT License is available at link:https://opensource.org/licenses/MIT[].
For purposes of the MIT License, "Software" will mean the Content.

If you did not receive this Content directly from the Eclipse Foundation, the Content is being redistributed by another party ("Redistributor") and different terms and conditions may apply to your use of any object code in the Content.
Check the Redistributor's license that was provided with the Content.
If no such license exists, contact the Redistributor.
Unless otherwise indicated below, the terms and conditions of the MIT License still apply to any source code in the Content and such source code may be obtained at link:https://www.eclipse.org[].
