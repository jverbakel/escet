//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008, 2023 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

// The ball sorting system takes balls as input and sorts them by their size. Balls enter the system as they are placed
// into the queue at the process gate. A lift brings the balls to a measuring station to determine their size. After
// measurement, balls are pushed out to a second lift. The second lift transports small balls to the first level, and
// big balls to the second level. A rotating hand transports the balls back to the home position.
//
// This is the original version of model, which matches the paper and the Supremica example model.

// Plants.

plant def Timer(event start, timeout):
  location q0:
    initial; marked;
    edge start goto q1;

  location q1:
    edge timeout goto q0;
    edge start;
end

controllable em_timer_measure_start, em_timer_before_put_start, em_timer_putting_start, em_timer_before_up_start,
             em_timer_before_ballcheck_start;
controllable es_timer_before_up_start, es_timer_before_deput_start, es_timer_before_ballcheck_start;
uncontrollable em_timer_measure_timeout, em_timer_before_put_timeout, em_timer_putting_timeout,
               em_timer_before_up_timeout, em_timer_before_ballcheck_timeout;
uncontrollable es_timer_before_up_timeout, es_timer_before_deput_timeout, es_timer_before_ballcheck_timeout;

em_timer_measure:          Timer(em_timer_measure_start,          em_timer_measure_timeout);
em_timer_before_put:       Timer(em_timer_before_put_start,       em_timer_before_put_timeout);
em_timer_putting:          Timer(em_timer_putting_start,          em_timer_putting_timeout);
em_timer_before_up:        Timer(em_timer_before_up_start,        em_timer_before_up_timeout);
em_timer_before_ballcheck: Timer(em_timer_before_ballcheck_start, em_timer_before_ballcheck_timeout);
es_timer_before_up:        Timer(es_timer_before_up_start,        es_timer_before_up_timeout);
es_timer_before_deput:     Timer(es_timer_before_deput_start,     es_timer_before_deput_timeout);
es_timer_before_ballcheck: Timer(es_timer_before_ballcheck_start, es_timer_before_ballcheck_timeout);

plant def Actuator(event go_in, go_out, is_in, is_out):
  location In:
    initial; marked;
    edge go_out goto Out;
    edge go_in, is_in;

  location Out:
    marked;
    edge go_in goto In;
    edge go_out, is_out;
end

controllable em_go_up, em_go_down;
uncontrollable em_is_up, em_is_down;

controllable gk_in_go_in, gk_in_go_out, gk_in_is_in, gk_in_is_out;
controllable gk_out_go_in, gk_out_go_out, gk_out_is_in, gk_out_is_out;
controllable em_putter_go_in, em_putter_go_out, em_putter_is_in, em_putter_is_out;

Elevator_measure: Actuator(em_go_up,        em_go_down,       em_is_up,        em_is_down);
Gatekeeper_in:    Actuator(gk_in_go_in,     gk_in_go_out,     gk_in_is_in,     gk_in_is_out);
Gatekeeper_out:   Actuator(gk_out_go_in,    gk_out_go_out,    gk_out_is_in,    gk_out_is_out);
Measure_putter:   Actuator(em_putter_go_in, em_putter_go_out, em_putter_is_in, em_putter_is_out);

plant def Button(event pressed, released, mode):
  location q0:
    initial; marked;
    edge pressed goto q1;

  location q1:
    marked;
    edge mode goto q2;

  location q2:
    marked;
    edge released goto q0;
end

controllable mode_automatic, mode_manual;
uncontrollable button_automatic_pressed, button_automatic_released, button_manual_pressed, button_manual_released;

Automatic_button: Button(button_automatic_pressed, button_automatic_released, mode_automatic);
Manual_button:    Button(button_manual_pressed,    button_manual_released,    mode_manual);

uncontrollable em_has_ball;

plant GK_EM:
  location q0:
    initial; marked;
    edge gk_out_go_out goto q1;

  location q1:
    marked;
    edge em_has_ball goto q0;
end

controllable arm_allocate_lower_buffer, arm_allocate_upper_buffer;

plant Disable_Events:
  location:
    initial; marked;
    edge arm_allocate_lower_buffer when false;
    edge arm_allocate_upper_buffer when false;
    edge button_automatic_pressed when false;
    edge em_putter_is_in when false;
    edge em_putter_is_out when false;
    edge gk_in_is_in when false;
    edge gk_in_is_out when false;
    edge gk_out_is_in when false;
    edge gk_out_is_out when false;
end

// Events not used in plant models.

plant def Selfloop(event e):
  location:
    initial; marked;
    edge e;
end

controllable em_measure_start, em_measure_allocate_station, em_measure_go_out, em_measure_go_in,
             em_measure_release_station;
uncontrollable em_large_ball, em_small_ball, em_measure_finished;

P1: Selfloop(em_measure_start);
P2: Selfloop(em_measure_allocate_station);
P3: Selfloop(em_measure_go_out);
P4: Selfloop(em_measure_go_in);
P5: Selfloop(em_measure_release_station);
P6: Selfloop(em_large_ball);
P7: Selfloop(em_small_ball);
P8: Selfloop(em_measure_finished);

controllable es_allocate_upper_buffer, arm_release_upper_buffer, arm_lower_upper_buffer, arm_raise_upper_buffer,
             es_release_upper_buffer, es_deput_ball_upper, es_put_ball_upper;

P10: Selfloop(es_allocate_upper_buffer);
P11: Selfloop(arm_release_upper_buffer);
P12: Selfloop(arm_lower_upper_buffer);
P13: Selfloop(arm_raise_upper_buffer);
P14: Selfloop(es_release_upper_buffer);
P15: Selfloop(es_deput_ball_upper);
P16: Selfloop(es_put_ball_upper);

controllable gk_enter_ball;

P17: Selfloop(gk_enter_ball);

uncontrollable gk_has_ball, gk_ball_out_of_gatekeeper;
controllable em_enter_ball, gk_release_ball;

p18: Selfloop(gk_has_ball);
p19: Selfloop(gk_ball_out_of_gatekeeper);
p20: Selfloop(em_enter_ball);
p21: Selfloop(gk_release_ball);

controllable es_allocate_lower_buffer, arm_release_lower_buffer, arm_lower_lower_buffer, arm_raise_lower_buffer,
             es_release_lower_buffer, es_deput_ball_lower, es_put_ball_lower;

p23: Selfloop(es_allocate_lower_buffer);
p24: Selfloop(arm_release_lower_buffer);
p25: Selfloop(arm_lower_lower_buffer);
p26: Selfloop(arm_raise_lower_buffer);
p27: Selfloop(es_release_lower_buffer);
p28: Selfloop(es_deput_ball_lower);
p29: Selfloop(es_put_ball_lower);

uncontrollable em_measure_has_ball;
controllable es_enter_ball;

p30: Selfloop(em_measure_has_ball);
p31: Selfloop(es_enter_ball);

controllable em_elevator_allocate_station, em_elevator_release_station, em_putter_start;
uncontrollable em_measure_has_no_ball, em_putter_finished, em_ball_out_of_measure_station;

p32: Selfloop(em_elevator_allocate_station);
p33: Selfloop(em_elevator_release_station);
p34: Selfloop(em_putter_start);
p35: Selfloop(em_measure_has_no_ball);
p36: Selfloop(em_putter_finished);
p37: Selfloop(em_ball_out_of_measure_station);

controllable es_got_ball, is_large_ball, is_small_ball, es_up_upper, es_up_lower, es_go_down, es_is_not_down;
controllable es_upper_putter_go_in, es_upper_putter_go_out, es_lower_putter_go_in, es_lower_putter_go_out;
uncontrollable es_is_upper, es_is_lower, es_no_ball_upper_level, es_no_ball_lower_level, es_ball_upper_level,
               es_ball_lower_level;

p38: Selfloop(es_got_ball);
p39: Selfloop(is_large_ball);
p40: Selfloop(is_small_ball);
p41: Selfloop(es_up_upper);
p42: Selfloop(es_up_lower);
p43: Selfloop(es_go_down);
p44: Selfloop(es_is_not_down);
p45: Selfloop(es_upper_putter_go_in);
p46: Selfloop(es_upper_putter_go_out);
p47: Selfloop(es_lower_putter_go_in);
p48: Selfloop(es_lower_putter_go_out);
p49: Selfloop(es_is_upper);
p50: Selfloop(es_is_lower);
p51: Selfloop(es_no_ball_upper_level);
p52: Selfloop(es_no_ball_lower_level);
p53: Selfloop(es_ball_upper_level);
p54: Selfloop(es_ball_lower_level);

controllable em_putter_allocate_station, em_putter_release_station;

p55: Selfloop(em_putter_allocate_station);
p56: Selfloop(em_putter_release_station);

// Requirements.

requirement Measure_station_measure:
  location q_0:
    initial; marked;
    edge em_measure_start goto q_0_1;

  location q_0_1:
    edge em_measure_allocate_station goto q_0_2;

  location q_0_2:
    edge em_measure_go_out goto q_1;

  location q_1:
    edge em_timer_measure_start goto q_2;

  location q_2:
    edge em_timer_measure_timeout goto q_3;

  location q_3:
    edge em_large_ball, em_small_ball goto q_4;

  location q_4:
    edge em_measure_go_in goto q_5;

  location q_5:
    edge em_measure_release_station goto q_6;

  location q_6:
    edge em_measure_finished goto q_0;
end

requirement ES_and_upper_buffer:
  location q_0:
    initial; marked;
    edge arm_allocate_upper_buffer goto q_1;
    edge es_allocate_upper_buffer goto q_2;

  location q_1:
    edge arm_release_upper_buffer goto q_0;
    edge arm_lower_upper_buffer, arm_raise_upper_buffer;

  location q_2:
    edge es_release_upper_buffer goto q_0;
    edge es_deput_ball_upper, es_put_ball_upper;
end

requirement Manual_or_automatic_mode:
  monitor mode_manual, mode_automatic;

  location q_0:
    initial; marked;
    edge mode_manual goto q_1;
    edge mode_automatic goto q_2;

  location q_1:
    edge gk_enter_ball goto q_0;

  location q_2:
    edge gk_enter_ball goto q_3;

  location q_3:
    edge gk_enter_ball goto q_4;

  location q_4:
    edge gk_enter_ball goto q_5;

  location q_5:
    edge gk_enter_ball goto q_6;

  location q_6:
    edge gk_enter_ball goto q_7;

  location q_7:
    edge gk_enter_ball goto q_0;
end

requirement GK_and_EM:
  location q_0:
    initial; marked;
    edge gk_has_ball goto q_1_0;

  location q_1_0:
    edge em_is_down goto q_1;

  location q_1:
    edge em_enter_ball goto q_2;

  location q_2:
    edge gk_release_ball goto q_3;

  location q_3:
    edge em_has_ball goto q_4;

  location q_4:
    edge gk_ball_out_of_gatekeeper goto q_5;

  location q_5:
    edge em_is_up goto q_0;
end

requirement ES_and_lower_buffer:
  location q_0:
    initial; marked;
    edge arm_allocate_lower_buffer goto q_1;
    edge es_allocate_lower_buffer goto q_2;

  location q_1:
    edge arm_release_lower_buffer goto q_0;
    edge arm_lower_lower_buffer, arm_raise_lower_buffer;

  location q_2:
    edge es_release_lower_buffer goto q_0;
    edge es_deput_ball_lower, es_put_ball_lower;
end

requirement EM_and_ES:
  location q_0:
    initial; marked;
    edge em_measure_has_ball goto q_1;

  location q_1:
    edge es_enter_ball goto q_0;
end

requirement Elevate_and_measure:
  location q_0:
    initial; marked;
    edge em_enter_ball goto q_0_1;

  location q_0_1:
    edge em_has_ball goto q_1;

  location q_1:
    edge em_timer_before_up_start goto q_2;

  location q_2:
    edge em_timer_before_up_timeout goto q_3_1;

  location q_3_1:
    edge em_elevator_allocate_station goto q_3;

  location q_3:
    edge em_go_up goto q_4;

  location q_4:
    edge em_is_up goto q_5_1;

  location q_5_1:
    edge em_elevator_release_station goto q_5;

  location q_5:
    edge em_timer_before_ballcheck_start goto q_6;

  location q_6:
    edge em_timer_before_ballcheck_timeout goto q_7;

  location q_7:
    edge em_measure_has_no_ball goto q_12;
    edge em_measure_has_ball goto q_8;

  location q_8:
    edge em_measure_start goto q_9_1;

  location q_9_1:
    edge em_measure_finished goto q_9;

  location q_9:
    edge em_putter_start goto q_11_1;

  location q_11_1:
    edge em_putter_finished goto q_11;

  location q_11:
    edge em_ball_out_of_measure_station goto q_12_1;

  location q_12_1:
    edge em_elevator_allocate_station goto q_12;

  location q_12:
    edge em_go_down goto q_13;

  location q_13:
    edge em_elevator_release_station goto q_0;
end

requirement Elevate_and_measure_block:
  location:
    initial; marked;
    edge em_timer_before_put_start when false;
    edge em_timer_before_put_timeout when false;
end

requirement Elevator_and_sort:
  location q_0:
    initial; marked;
    edge es_enter_ball goto q_0_1;

  location q_0_1:
    edge es_got_ball goto q_1;

  location q_1:
    edge es_timer_before_up_start goto q_2;

  location q_2:
    edge es_timer_before_up_timeout goto q_3;

  location q_3:
    edge is_large_ball goto q_4_upper;
    edge is_small_ball goto q_4_lower;

  location q_4_upper:
    edge es_up_upper goto q_5_upper;

  location q_5_upper:
    edge es_is_upper goto q_6_upper;

  location q_6_upper:
    edge es_timer_before_ballcheck_start goto q_7_upper;

  location q_7_upper:
    edge es_timer_before_ballcheck_timeout goto q_8_upper_1;

  location q_8_upper_1:
    edge es_no_ball_upper_level goto q_12;
    edge es_ball_upper_level goto q_8_upper;

  location q_8_upper:
    edge es_allocate_upper_buffer goto q_8_upper_2;

  location q_8_upper_2:
    edge es_upper_putter_go_out goto q_9_upper;

  location q_9_upper:
    edge es_timer_before_deput_start goto q_10_upper;

  location q_10_upper:
    edge es_timer_before_deput_timeout goto q_11_upper;

  location q_11_upper:
    edge es_upper_putter_go_in goto q_11_upper_1;

  location q_11_upper_1:
    edge es_release_upper_buffer goto q_12;

  location q_4_lower:
    edge es_up_lower goto q_5_lower;

  location q_5_lower:
    edge es_is_lower goto q_6_lower;

  location q_6_lower:
    edge es_timer_before_ballcheck_start goto q_7_lower;

  location q_7_lower:
    edge es_timer_before_ballcheck_timeout goto q_8_lower_1;

  location q_8_lower_1:
    edge es_no_ball_lower_level goto q_12;
    edge es_ball_lower_level goto q_8_lower;

  location q_8_lower:
    edge es_allocate_lower_buffer goto q_8_lower_2;

  location q_8_lower_2:
    edge es_lower_putter_go_out goto q_9_lower;

  location q_9_lower:
    edge es_timer_before_deput_start goto q_10_lower;

  location q_10_lower:
    edge es_timer_before_deput_timeout goto q_11_lower;

  location q_11_lower:
    edge es_lower_putter_go_in goto q_11_lower_1;

  location q_11_lower_1:
    edge es_release_lower_buffer goto q_12;

  location q_12:
    edge es_go_down goto q_0;
end

requirement Elevator_and_sort_block:
  location:
    initial; marked;
    edge es_is_not_down when false;
end

requirement Measure_station_putter:
  location q_0:
    initial; marked;
    edge em_putter_start goto q_1;

  location q_1:
    edge em_putter_allocate_station goto q_2;

  location q_2:
    edge em_putter_go_out goto q_4_1;

  location q_4_1:
    edge em_timer_putting_start goto q_4_2;

  location q_4_2:
    edge em_timer_putting_timeout goto q_4;

  location q_4:
    edge em_putter_go_in goto q_5;

  location q_5:
    edge em_putter_release_station goto q_6;

  location q_6:
    edge em_putter_finished goto q_0;
end

requirement Measure_station:
  location q_0:
    initial; marked;
    edge em_measure_allocate_station goto q_1;
    edge em_putter_allocate_station goto q_2;
    edge em_elevator_allocate_station goto q_3;

  location q_1:
    edge em_measure_release_station goto q_0;
    edge em_measure_go_in, em_measure_go_out;

  location q_2:
    edge em_putter_release_station goto q_0;
    edge em_putter_go_in, em_putter_go_out;

  location q_3:
    edge em_elevator_release_station goto q_0;
    edge em_go_up, em_go_down;
end

requirement Size_of_ball:
  location q_0:
    initial; marked;
    edge em_large_ball goto q_1;
    edge em_small_ball goto q_2;
    edge is_large_ball, is_small_ball;

  location q_1:
    marked;
    edge em_small_ball goto q_2;
    edge is_large_ball;

  location q_2:
    marked;
    edge em_large_ball goto q_1;
    edge is_small_ball;
end

requirement Gatekeeper:
  location q_0:
    initial; marked;
    edge gk_enter_ball goto q_1_1;

  location q_1_1:
    edge gk_in_go_out goto q_1;

  location q_1:
    edge gk_has_ball goto q_2;

  location q_2:
    edge gk_in_go_in goto q_3;

  location q_3:
    edge gk_release_ball goto q_4;

  location q_4:
    edge gk_out_go_out goto q_5;

  location q_5:
    edge gk_ball_out_of_gatekeeper goto q_6;

  location q_6:
    edge gk_out_go_in goto q_0;
end
